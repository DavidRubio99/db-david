<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\TableController;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::group(["middleware" => ['auth:sanctum', 'verified']],function(){
    
    Route::get('/dashboard', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');

    Route::resource("/users", UserController::class);
    Route::resource("/roles", RoleController::class);
    Route::resource("/columns", ColumnController::class);
    Route::resource("/tables", TableController::class);
    Route::resource("/temporadas", TableController::class);
    Route::resource("/menus", TableController::class);
    Route::resource("/tipos", TableController::class);
    Route::resource("/journals", TableController::class);

});


