<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'class_name',
        'controller',
        'url',
        'url_id',
        'plural',
        'singular'
    ];

    public function Columns() {
        return $this->hasMany(Column::class);
    }  

    public function action(){
        return $this->belongsToMany(Action::class);
    }

    public function journal(){
        return $this->hasMany(Table::class);  
    
    }
}
