<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'table_id'
    ];

    public function users()
    {
        return $this->hasMany(User::class);
    }    

    public function table()
    {
        return $this->belongsTo(Table::class);
    }  
}
