<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Journal extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'fecha',
        'table_id',
        'user_id',
        'reg_id',
        'action'
        
    ];

    public function table(){
        return $this->belongsTo(Table::class);  
    
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

}
