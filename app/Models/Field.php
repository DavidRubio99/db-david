<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'name',
        'view_id',
        'column_id'
        
    ];

    public function view() {
        return $this->belongsTo(View::class);
    }    
    public function column() {
        return $this->belongsTo(View::class);
    }    
}
