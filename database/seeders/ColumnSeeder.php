<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Column;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class ColumnSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->setUserColumns();
        $this->setRoleColumns();
        $this->setTableColumns();
        $this->setColumnColumns();
        $this->setViewColumns();
    }
    private function setUserColumns() {
      
                Column::create([
                    'table_id' => 1,   // users
                    'name' => 'id',
                ]);
                Column::create([
                    'table_id' => 1,   
                    'name' => 'name',
                ]);
        
        
            }
        
            private function setRoleColumns() {
                Column::create([
                    'table_id' => 2,    //roles
                    'name' => 'id',
                ]);
                Column::create([
                    'table_id' => 2,
                    'name' => 'name',
                ]);            
            }
        
            private function setTableColumns() {
                Column::create([
                    'table_id' => 3,    //tables
                    'name' => 'id',
                ]);
                Column::create([
                    'table_id' => 3,
                    'name' => 'name',
                ]);
                Column::create([
                    'table_id' => 3,
                    'name' => 'class_name',
                ]);                        
                Column::create([
                    'table_id' => 3,
                    'name' => 'controller',
                ]);                        
                Column::create([
                    'table_id' => 3,
                    'name' => 'url',
                ]);                        
                Column::create([
                    'table_id' => 3,
                    'name' => 'url_id',
                ]);                        
                Column::create([
                    'table_id' => 3,
                    'name' => 'plural',
                ]);                    
                Column::create([
                    'table_id' => 3,
                    'name' => 'singular',
                ]);                    
            }
        
            private function setColumnColumns() {
                Column::create([
                    'table_id' => 4,    //columns
                    'name' => 'id',
                ]);
                Column::create([
                    
                    'table_id' => 4,
                    'name' => 'table_id',
                ]);
                Column::create([
                    
                    'table_id' => 4,
                    'name' => 'name',
                ]);
            }

            private function setViewColumns() {
                Column::create([
                    
                    'table_id' => 5,
                    'name' => 'id'
                ]);
                Column::create([
                    
                    'table_id' => 5,
                    'name' => 'table_id'
                ]);
                Column::create([
                    
                    'table_id' => 5,
                    'name' => 'type'
                ]);
                Column::create([
                    
                    'table_id' => 5,
                    'name' => 'content'
                ]);
            }
}

