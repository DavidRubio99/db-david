<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;
use Illuminate\Support\Str;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'id' => 1,
            'table_id' => 2,
            'name' => 'Superadmin',
        ]);

        Role::create([
            'id' => 2,
            'table_id' => 2,
            'name' => 'Admin',
        ]);

        Role::create([
            'id' => 3,
            'table_id' => 2,
            'name' => 'User',
        ]);
    }
}
