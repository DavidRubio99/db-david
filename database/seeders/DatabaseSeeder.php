<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            TableSeeder::class,
            RoleSeeder::class,
            UserSeeder::class,
            ColumnSeeder::class,
            MenuSeeder::class,
            TipoSeeder::class,
            ViewSeeder::class,
            TemporadaSeeder::class,
            ActionSeeder::class,            
        ]);
    }
}
