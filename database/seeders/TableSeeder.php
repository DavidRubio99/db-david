<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Table;
use Illuminate\Support\Str;

class TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Table::create([
            'id' => 1,
            'name' => 'users',
            'class_name' => 'User',
            'controller' => 'UserController',
            'url' => 'users',
            'url_id' => 'user',
            'plural' => 'Usuarios',
            'singular' => 'Usuario',
        ]);
        Table::create([
            'id' => 2,
            'name' => 'roles',
            'class_name' => 'Role',
            'controller' => 'RoleController',
            'url' => 'roles',
            'url_id' => 'role',
            'plural' => 'Perfiles',
            'singular' => 'Perfil',          
        ]);
        Table::create([
            'id' => 3,
            'name' => 'tables',
            'class_name' => 'Table',
            'controller' => 'TableController',
            'url' => 'tables',
            'url_id' => 'table',
            'plural' => 'Tablas',
            'singular' => 'Tablas',            
        ]);
        Table::create([
            'id' => 4,
            'name' => 'columns',
            'class_name' => 'Column',
            'controller' => 'ColumnController',
            'url' => 'columns',
            'url_id' => 'column',
            'plural' => 'Columnas',
            'singular' => 'Columna',
        ]);
        Table::create([
            'id' => 5,
            'name' => 'view',
            'class_name' => 'View',
            'controller' => 'ViewController',
            'url' => 'views',
            'url_id' => 'view',
            'plural' => 'Views',
            'singular' => 'View',
        ]);
        Table::create([
            'id' => 6,
            'name' => 'field',
            'class_name' => 'Field',
            'controller' => 'FieldController',
            'url' => 'fields',
            'url_id' => 'field',
            'plural' => 'Fields',
            'singular' => 'Field',
        ]);
        Table::create([
            'id' => 7,
            'name' => 'temporada',
            'class_name' => 'Temporada',
            'controller' => 'TemporadaController',
            'url' => 'temporadas',
            'url_id' => 'temporada',
            'plural' => 'Temporadas',
            'singular' => 'Temporada',
        ]);
        Table::create([
            'id' => 8,
            'name' => 'menu',
            'class_name' => 'Menu',
            'controller' => 'MenuController',
            'url' => 'menus',
            'url_id' => 'menu',
            'plural' => 'Menus',
            'singular' => 'Menu',
        ]);
        Table::create([
            'id' => 9,
            'name' => 'tipo',
            'class_name' => 'Tipo',
            'controller' => 'TipoController',
            'url' => 'tipos',
            'url_id' => 'tipo',
            'plural' => 'Tipos',
            'singular' => 'Tipo',
        ]);
        Table::create([
            'id' => 10,
            'name' => 'journal',
            'class_name' => 'Journal',
            'controller' => 'JournalController',
            'url' => 'journals',
            'url_id' => 'journal',
            'plural' => 'Journals',
            'singular' => 'Journal',
        ]);
    }
}
