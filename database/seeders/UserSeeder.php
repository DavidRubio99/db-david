<?php

namespace Database\Seeders;


use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'id' => 1,
            'table_id' => 1,
            'name' => 'david',
            'first_name' => 'David',
            'last_name' => 'Rubio',
            'email' => 'david_rubio_atroche@hotmail.com',
            'password' => Hash::make('Xmlns_99'),
            'role_id'=> 1,
        ]);
        User::create([
            'id' => 2,
            'table_id' => 1,
            'name' => 'gustavo',
            'first_name' => 'Gustavo',
            'last_name' => 'Miranda',
            'email' => 'info@magusinformatica.com',
            'password' => Hash::make('Xmlns_99'),
            'role_id'=> 2,

        ]);    
        User::create([
            'id' => 3,
            'table_id' => 1,
            'name' => 'pilar',
            'first_name' => 'Pilar',
            'last_name' => 'Ruiz',
            'email' => 'mpruiz@gmail.com',
            'password' => Hash::make('Xmlns_99'),
            'role_id'=> 3,
            
        ]);    
    }
}
