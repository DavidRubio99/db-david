<?php

namespace Database\Seeders;

use App\Models\Action;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;


class ActionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $actions=['index','show','create','edit','update','delete'];
        foreach($actions as $action){
            Action::create([
                'name' => $action,
            ]);
        }
    }
}
